FROM openjdk:8-alpine-with-tzdata
MAINTAINER hino<sinhngay3110@gmail.com>

COPY target/*.jar app.jar

ENTRYPOINT ["java","-jar","/app.jar"]
